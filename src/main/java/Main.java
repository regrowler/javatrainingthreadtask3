import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

public class Main {
    final static String hash1 = "4446617406dd994f85a4ef3c3be5aa0d";
    final static String hash2 = "DDC4035FF6451F85BB796879E9E5EA49";
    final static String hash3 = "ddc4035ff6451f85bb796879e9e5ea49";
    static int BUF_SIZE = 100;
    ArrayBlockingQueue<String> queue = new ArrayBlockingQueue(BUF_SIZE);
    List<String> buf = new ArrayList<>();
    AtomicBoolean processing = new AtomicBoolean(true);

    public static void main(String[] d) {
        Main main = new Main();
        try {
            long  t1=System.currentTimeMillis();
            main.start(8);
            long t2=System.currentTimeMillis();
            double t=(t2-t1)/1000/60;
            System.out.println("time: "+t);
        } catch (Exception e) {
            e.printStackTrace();
            main.stop();
        }

    }

    public Main() {
        Stream.generate(() -> new Thread(new TreadHasher())).limit(4).forEach(thread -> thread.start());
    }

    public void stop() {
        processing.set(false);
    }

    public void start(int d) {
        for (int i = 1; i <= d; i++) {
            addAll2("", i);
        }
//        addAll("", d);
    }

    public void addAll(String s, int d) {
        if (d >= 0) {
            Hasher hasher = Hashing.md5().newHasher();
            hasher.putString(s, StandardCharsets.UTF_8);
            String hash = hasher.hash().toString();
            if (hash.equals(hash1.toLowerCase())) {
                System.out.println("found " + s + " " + hash + " " + hash1.toLowerCase());
                processing.set(false);
            } else {
                System.out.println("wrong " + s + " " + hash + " " + hash1.toLowerCase());
            }
            for (char f = 'a'; f <= 'z'; f++) {
//                System.out.println(s+f);
                addAll(s + f, d - 1);
            }
        }
    }

    public void addAll2(String s, int d) {
        if (d >= 0) {
            add(s);
            for (char f = 'a'; f <= 'z' && processing.get(); f++) {
//                System.out.println(s+f);
                addAll2(s + f, d - 1);
            }
        }
    }

    public void add(String s) {
        if (buf.size() < BUF_SIZE) {
            buf.add(s);
        } else {
            if (queue.size() == 0) {
                queue.addAll(buf.subList(0, BUF_SIZE));
                buf.clear();
                buf.add(s);
            } else {
                buf.add(s);
            }
        }
    }

    private class TreadHasher implements Runnable {


        @Override
        public void run() {
            try {
                long count = 0;
                while (processing.get()) {
                    String pas = queue.take();
                    count++;
                    Hasher hasher = Hashing.md5().newHasher();
                    hasher.putString(pas, StandardCharsets.UTF_8);
                    String hash = hasher.hash().toString();
                    if (hash.equals(hash3)) {
                        synchronized (processing) {
                            processing.set(false);
                        }
                        System.out.println("found " + pas + " " + hash + " " + hash3.toLowerCase());

                    } else {
                        //System.out.println("wrong " + pas+" "+hash+" "+hash1.toLowerCase());
                    }

                }
                System.out.println(Thread.currentThread().getName() + " stopped checked " + count + " passwords");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

    }
}
